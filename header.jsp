<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<header class="main-header" id="header">

	<!-- Logo -->
	<div id="logo-group">
		<span id="logo"> <img src="resources/custom/img/riyad-bank-logo.png"
			alt="Saudi Industrial Development Fund">
		</span>
		 <span  id="activity" class="activity-dropdown"> <i
			class="fa fa-user"></i> <b class="badge">
				<div id="result"></div>
		</b>
		</span>

		<!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
		<div class="ajax-dropdown">

			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a data-toggle="tab" href="#home"
					style="color: black;"><fmt:message key="page.header.notification" /></a></li>
				<li><a data-toggle="tab" href="#memo" style="color: black;"><fmt:message key="page.header.remind" /></a></li>
				<li><a data-toggle="tab" href="#userGuide"
					style="color: black;"><fmt:message key="page.header.userGuide" /></a></li>
			</ul>

			<div class="tab-content" id="notify">
				<div id="home" class="tab-pane fade in active">

					<ul class="notification-body" id="unReadCrspNotifcation">

					</ul>
				</div>
				<div id="memo" class="tab-pane fade">
					<ul class="notification-body" id="unReadImportantCrspNotifcation">

					</ul>

				</div>


				<div id="userGuide" class="tab-pane fade">
				  <ul class="userguide-drop">
					       <li class="userguide">
	                        	<span class="bg-color-purple" url="http://fncinclu.sidf.gov.sa:9080/navigator/bookmark.jsp?
	                        						desktop=SSS&repositoryId=ProdRepository&repositoryType=p8&docid=Document%2C%7BC7731427-6A1B-4055-9AF0-0186CFCE7EE4%7D%2C%7B1973B329-2ACA-C2F5-85A4-604AC8500000%7D&mimeType=application%2Fvnd.openxmlformats-officedocument.wordprocessingml.document&template_name=Document&version=released&vsId=%7BD3B4C428-6DE8-CE97-8463-604AC8500000%7D">
	                        						<i class="fa fa-file-word-o"></i></span><b>دليل مستخدم الاتصالات الإدارية (عربي)</b>
	                       </li>
	                       <li class="userguide">
	                        	<span class="bg-color-redLight" url="http://fncinclu.sidf.gov.sa:9080/navigator/bookmark.jsp?
	                        						desktop=SSS&repositoryId=ProdRepository&repositoryType=p8&docid=Document%2C%7BC7731427-6A1B-4055-9AF0-0186CFCE7EE4%7D%2C%7BB4DC3940-18F8-CD70-8CEA-604AC8500000%7D&mimeType=application%2Fvnd.openxmlformats-officedocument.wordprocessingml.document&template_name=Document&version=released&vsId=%7B2F5EE7C0-6982-CF5A-BC81-604AC8500000%7D">
	                        						<i class="fa fa-file-word-o"></i></span><b>دليل مستخدم الاتصالات الإدارية (انجليزي)</b>
	                       </li>
	                       <li class="userguide">
	                        	<span class="bg-color-teal" url="http://fncinclu.sidf.gov.sa:9080/navigator/bookmark.jsp?
	                        						desktop=SSS&repositoryId=ProdRepository&repositoryType=p8&docid=Folder%2C%7BC7731427-6A1B-4055-9AF0-0186CFCE7EE4%7D%2C%7BB38D00C5-F6A2-C857-8718-604AC7B00000%7D&mimeType=folder&template_name=Folder">
	                        						<i class="fa fa-file-video-o"></i></span><b>دليل مستخدم الاتصالات الإدارية فيديو </b>
	                       </li>
	                       <li class="userguide">
	                        	<span class="bg-color-teal" url="http://fncinclu.sidf.gov.sa:9080/navigator/bookmark.jsp?desktop=SIDFADMIN&repositoryId=ProdRepository&repositoryType=p8&docid=Document%2C%7BC7731427-6A1B-4055-9AF0-0186CFCE7EE4%7D%2C%7B41F6CF17-7729-CDF5-8547-60FF44100000%7D&mimeType=application%2Fpdf&template_name=Document&version=released&vsId=%7B63A96812-10CC-C6E9-861B-60FF44100000%7D">
	                        	</i></span><b>دليل المستخدم للصادر الورقى </b>
	                       </li>
	                       <li class="userguide">
	                        	<span class="bg-color-purple" url="http://fncinclu.sidf.gov.sa:9080/navigator/bookmark.jsp?
	                        						desktop=SSS&repositoryId=ProdRepository&repositoryType=p8&docid=Document%2C%7BC7731427-6A1B-4055-9AF0-0186CFCE7EE4%7D%2C%7BA508BFB1-6043-CE62-863F-604AC8100000%7D&mimeType=application%2Fvnd.openxmlformats-officedocument.wordprocessingml.document&template_name=Document&version=released&vsId=%7B4AE005FF-A4B1-CF67-8546-604AC8100000%7D">
	                        						<i class="fa fa-file-word-o"></i></span><b>دليل مستخدم المحتوى الشامل (عربي)</b>
	                       </li>
	                       <li class="userguide">
	                        	<span class="bg-color-redLight" url="http://fncinclu.sidf.gov.sa:9080/navigator/bookmark.jsp?
	                        						desktop=SSS&repositoryId=ProdRepository&repositoryType=p8&docid=Document%2C%7BC7731427-6A1B-4055-9AF0-0186CFCE7EE4%7D%2C%7B46CA7318-15D0-CDB5-8D34-604AC8100000%7D&mimeType=application%2Fvnd.openxmlformats-officedocument.wordprocessingml.document&template_name=Document&version=released&vsId=%7B433AEACD-BF70-C9B6-BC72-604AC8100000%7D">
	                        						<i class="fa fa-file-word-o"></i></span><b>دليل مستخدم المحتوى الشامل (انجليزي)</b>
	                       </li>
	                       <li class="userguide">
	                        	<span class="bg-color-teal" url="http://fncinclu.sidf.gov.sa:9080/navigator/bookmark.jsp?
	                        						desktop=SSS&repositoryId=ProdRepository&repositoryType=p8&docid=Folder%2C%7BC7731427-6A1B-4055-9AF0-0186CFCE7EE4%7D%2C%7BDE7729DD-A952-C319-85A0-604AC7D00000%7D&mimeType=folder&template_name=Folder">
	                        						<i class="fa fa-file-video-o"></i></span><b>دليل مستخدم المحتوى الشامل فيديو </b>
	                       </li>
	                       
<!--                       <li> -->
<!--                         <span class="bg-color-purple"><i class="fa fa-paperclip"></i></span><b><fmt:message key="page.header.originalAttachments" /></b> -->
<!--                       </li> -->
<!-- 					  <li> -->
<!--                         <span class="bg-color-redLight"><i class="fa fa-bell"></i></span> <b><fmt:message key="page.header.additionalAttachments" /></b> -->
<!--                       </li> -->
<!-- 					  <li> -->
<!--                         <span class="bg-color-teal"><i class="fa fa-file-text-o"></i></span><b><fmt:message key="page.header.InternalNote" /></b> -->
<!--                       </li> -->
<!-- 					  <li> -->
<!--                         <span class="bg-color-yellow"><i class="icon_mail-unread"></i></span><b><fmt:message key="page.header.correspondenceUnread" /></b> -->
<!--                       </li> -->
<!-- 					   <li> -->
<!--                         <span class="bg-color-blue"><i class="icon_mail-read-o"></i></span><b><fmt:message key="page.header.correspondenceRead" /></b> -->
<!--                       </li> -->
<!-- 					    <li> -->
<!--                         <span class="bg-color-pink"><i class="icon_chat_line"></i></span><b></span><b><fmt:message key="page.header.ReadMessage" /></b> -->
<!--                       </li> -->
<!-- 					   <li> -->
<!--                         <span class="bg-color-gold"><i class="icon_note"></i></span><b>طلب مقروءة</b> -->
<!--                       </li> -->
					  </ul>
				</div>
			</div>

			<!-- notification content -->
			<!-- <div class="ajax-notifications custom-scroll">

						<div class="alert alert-transparent">
							<h4>Click a button to show messages here</h4>
							This blank page message helps protect your privacy, or you can show the first message here automatically.
						</div>

						<i class="fa fa-lock fa-4x fa-border"></i>

					</div> -->
			<!-- end notification content -->

			<!-- footer: refresh area -->
			<span><label id="refreshDate"></label>
	
				<button type="button" id="refreshBTN"
					data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..."
					class="btn btn-xs btn-default pull-right">
					<i class="fa fa-refresh"></i>
				</button>
		
			</span>
			<!-- end footer -->

		</div>
		<!-- END AJAX-DROPDOWN -->
	</div>

	<!-- <div style="width:300px; padding-top: 10;">
	<marquee DIRECTION="RIGHT" style="color: red;">تم اضافة دليل المستخدم للتصدير الورقى</marquee>
	</div> -->
	<!-- pulled right: nav area -->
	<div id="leftHeader" class="pull-right">

		<!-- collapse menu button -->
		<div id="hide-menu" class="btn-header pull-right" style="margin-left: 20px;">
			<span> <a href="javascript:void(0);" data-action="toggleMenu"
				title="Collapse Menu"><i class="fa fa-reorder"></i></a>
			</span>
		</div>
		<!-- end collapse menu -->

		<!-- #MOBILE -->
		<!-- Top menu profile link : this shows only when top menu is active -->
		<ul id="mobile-profile-img"
			class="header-dropdown-list hidden-xs padding-5">
			<li class=""><a href="#"
				class="dropdown-toggle no-margin userdropdown"
				data-toggle="dropdown"> <img
					src="" alt=""
					class="online" />
			</a>
				<ul class="dropdown-menu pull-right">
					<li><a href="javascript:void(0);"
						class="padding-10 padding-top-0 padding-bottom-0"><i
							class="fa fa-cog"></i> Setting</a></li>
					<li class="divider"></li>
					<li><a href="profile.html"
						class="padding-10 padding-top-0 padding-bottom-0"> <i
							class="fa fa-user"></i> <u>P</u>rofile
					</a></li>
					<li class="divider"></li>
					<li><a href="javascript:void(0);"
						class="padding-10 padding-top-0 padding-bottom-0"
						data-action="toggleShortcut"><i class="fa fa-arrow-down"></i>
							<u>S</u>hortcut</a></li>
					<li class="divider"></li>
					<li><a href="javascript:void(0);"
						class="padding-10 padding-top-0 padding-bottom-0"
						data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i>
							Full <u>S</u>creen</a></li>
					<li class="divider"></li>
					<li>
						<!-- <a href="logout.go" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a> -->
						<a href="logout.go"
						class="padding-10 padding-top-5 padding-bottom-5"><i
							class="fa fa-sign-out fa-lg"></i> <strong><fmt:message key="page.header.ReadMessage" /></strong></a>
					</li>
				</ul></li>
		</ul>

		<!-- logout button -->
		<div id="logout" class="btn-header transparent pull-right">
			<span> <a href="logout.go" title="<fmt:message key="page.header.logout" />"
				data-action="userLogout"
				data-logout-msg="<fmt:message key="page.header.logoutMessage" />"><i
					class="fa fa-sign-out"></i></a>
			</span>
			<!-- <span> <a href="logout.go" title="Sign Out"><i class="fa fa-sign-out"></i></a> </span> -->
		</div>
		<!-- end logout button -->

		<!-- search mobile button (this is hidden till mobile view port) -->
		<div id="search-mobile" class="btn-header transparent pull-right">
			<span> <a href="javascript:void(0)" title="Search"><i
					class="fa fa-search"></i></a>
			</span>
		</div>
		<!-- end search mobile button -->

		<!-- input: search field -->
		<form  class="header-search pull-right" method="POST">
			<input id="search-fld" type="text" name="param"
				placeholder="<fmt:message key="page.header.Search" />"
				data-autocomplete='[
					"ActionScript",
					"AppleScript",
					"Asp",
					"BASIC",
					"C"	]'>
			<button type="submit" id="globalSearch" >
				<i class="fa fa-search"></i>
			</button>
			<a href="javascript:void(0);" id="cancel-search-js"
				title="Cancel Search"><i class="fa fa-times"></i></a>
		</form>
		<!-- end input: search field -->

		<!-- fullscreen button -->
		<div id="fullscreen" class="btn-header transparent pull-right">
			<span> <a href="javascript:void(0);"
				data-action="launchFullscreen" title="<fmt:message key="page.header.fullScreen" />"><i
					class="fa fa-arrows-alt"></i></a>
			</span>
		</div>
		<!-- end fullscreen button -->

		<!-- multiple lang dropdown : find all flags in the flags page -->
		<a id="redirectLanguageAnchor" class="o-link" hidden="true"></a>
		<ul id="localeMenu" class="header-dropdown-list">
			<li><a id="languageLabel" href="#" class="dropdown-toggle"
				data-toggle="dropdown"> <c:choose>
						<c:when test="${localeCode== constantsMap.LANG_AR}">
							<img src="resources/custom/img/blank.gif" class="flag flag-sa">
							<span><fmt:message
									key="sideSettings.main.selectLanguage.arabic" /></span>
						</c:when>
						<c:otherwise>
							<img src="resources/custom/img/blank.gif" class="flag flag-us">
							<span><fmt:message
									key="sideSettings.main.selectLanguage.english" /></span>
						</c:otherwise>
					</c:choose> <i class="fa fa-angle-down"></i>
			</a>
				<ul class="dropdown-menu pull-right">
					<c:choose>
						<c:when test="${localeCode== constantsMap.LANG_AR}">
							<li class="active">
						</c:when>
						<c:otherwise>
							<li class="">
						</c:otherwise>
					</c:choose>
					<a class="languageSelector" href="javascript:void(0);"><img
						src="resources/custom/img/blank.gif" lang="<%=Constants.LANG_AR%>"
						class="flag flag-sa" alt="Saudi Arabia"><fmt:message key="page.header.Arabic" /></a></li>
			<c:choose>
				<c:when test="${localeCode== constantsMap.LANG_AR}">
					<li class="">
				</c:when>
				<c:otherwise>
					<li class="active">
				</c:otherwise>
			</c:choose>
			<a class="languageSelector" href="javascript:void(0);"><img
				src="resources/custom/img/blank.gif" lang="<%=Constants.LANG_EN%>"
				class="flag flag-us" alt="United States"><fmt:message key="page.header.English" /></a>
			</li>
		</ul>
		</li>
		</ul>
		<!-- end multiple lang -->

	</div>

	<!-- end pulled right: nav area -->
</header>